## Trained models for predicting cold-responsive genes in grass species

- Single-species folder:

- Pg-100.RDS: the model was trained in pearl millet used sequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Pm-100.RDS: the model was trained in proso millet used sequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Pv-100.RDS: the model was trained in switchgrass usedsequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Sb-100.RDS: the model was trained in sorghum used sequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Si-100.RDS: the model was trained in foxtail millet used sequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Zm-100.RDS: the model was trained in maize used sequence features and the framework described in the within species prediction model in the paper, the number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers.;

- Four-species folder:

- Fourspecies-100.RDS: the model was trained using subsampled genes of four grass species (pearl millet, proso millet, foxtail millet and switchgrass) from pooled genes in six species (pearl millet, proso millet, foxtail millet, switchgrass, sorghum and maize) and sequence features. Then the model was trained using the framework described in the cross species prediction model. The number 100 indicates a random seed selected for splitting 90% data for training. The same for other labeled numbers..

- Note: All of deposited models were depend on one specific random seed for splitting training and holdout testing data from the total data.

## Using the trained model to predict cold-responsive genes in another species
### Prepared your data with sequence features per gene using GenomeFeature. 

```
> source("../R scripts/MLfunctions.R")
> fseq = readRDS("Four-species/FourSpecies-100.RDS") # select one of training models
> x = read.csv("../output.txt") # output is the raw feature output using GenomeFeature module, but retaining expressed genes in the prediction will avoid noise during the prediction. You have to follow instructions on the main page to obtain expressed genes from your annotation file. 
> mytest = preData(x) # imputation and scale 
> mypred = predict(fseq$model, mytest)
> table(mypred) # displaying the number of predicted DEGs and nonDEGs
> names(mypred) = rownames(mytest) # you will know the class of cold/noncold-responsive genes in mypred.
```
