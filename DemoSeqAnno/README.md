# demo annotation and sequence file for running "GenomeFeature" to extract sequence features per gene

- Sbicolor_454_v3.0.1.fa: demo fa file of sorghum
- Sbicolor_454_v3.1.1.gene.primary.gff3: demo gff3 file of sorghum with primary transcript per gene
