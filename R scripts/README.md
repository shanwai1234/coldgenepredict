# R scripts for running machine learning models

- MLfunctions.R: functions deposited in R for running machine learning models.
- RunML-withinspecies.R: example codes for running machine learning models for within-species predictions.
- RunML-crossspecies.R: example codes for running machine learning models for cross-species predictions. 