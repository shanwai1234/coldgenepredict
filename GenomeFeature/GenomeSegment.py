#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from pyfaidx import Fasta
from GenomeFeature import GeneExtractor as ge

def revcmp_str (input_str):
    rcs = input_str[::-1]
    rcs = rcs.replace('A','X')
    rcs = rcs.replace('T','A')
    rcs = rcs.replace('X','T')
    rcs = rcs.replace('C','X')
    rcs = rcs.replace('G','C')
    rcs = rcs.replace('X','G')
    return rcs 

def upstream(fa, ann, kb1, kb2):
    """
    Extracting gene upstream sequences. 
    fa is genome assembly file; 
    ann is the annotation file; 
    kb1 is the defined length of 5' UTR;
    kb2 is the defined length of upstream.
    """
    f = Fasta(fa)
    with open(ann, 'r') as fh, open('5-UTR.fa', 'w') as out1, open('upstream.fa', 'w') as out2:
        mdict = {}
        ndict = {}
        for line in fh:
            # this is the demo line that we want to filter out
            # chr7    GLEAN   Gene    25420153        25421713        0.953889        -       .       Name=Pgl_GLEAN_10006696;
            if line.startswith('#'):
                continue
            new = line.strip().split('\t')
            if new[2] != 'CDS':
                continue
            gene = ge.GeneName(new[-1])
            if gene not in mdict:
                mdict[gene] = []
                ndict[gene] = []
            ndict[gene].append(new[0])
            ndict[gene].append(new[6])
            mdict[gene].append(int(new[3]))
            mdict[gene].append(int(new[4]))

        for gene in sorted(mdict):
            # when gene is on the positive strand
            if ndict[gene][1] == '+':
                start = min(mdict[gene])
                start1 = start - (int(kb1) + 1)
                stop1 = start - 1
                start2 = start1 - (int(kb2) + 1)
                stop2 = start1 - 1
                chrom = ndict[gene][0]
                if start1 < 0:
                    start1 = 0
                if stop1 < 0:
                    stop1 = 0
                if start2 < 0:
                    start2 = 0
                if stop2 < 0:
                    stop2 = 0
                k1 = str(f[chrom][start1:stop1])
                out1.write('>{0}-5UTR'.format(gene) + '\n')
                out1.write(k1 + '\n')
                k2 = str(f[chrom][start2:stop2])
                out2.write('>{0}-upstream'.format(gene) + '\n')
                out2.write(k2 + '\n')
            # when gene is on the negative strand
            elif ndict[gene][1] == '-':
                stop = max(mdict[gene])
                start1 = stop + 1
                stop1 = stop + (int(kb1) + 1)
                start2 = stop1 + 1
                stop2 = stop1 + (int(kb2) + 1)
                k1 = revcmp_str(str(f[chrom][start1:stop1]))
                out1.write('>{0}-5UTR'.format(gene) + '\n')
                out1.write(k1 + '\n')
                k2 = revcmp_str(str(f[chrom][start2:stop2]))
                out2.write('>{0}-upstream'.format(gene) + '\n')
                out2.write(k2 + '\n')

def downstream(fa, ann, kb1, kb2):
    '''
    Extracting gene upstream sequences. fa is genome assembly file, ann is
    the annotation file, kb1 is the defined length of 3' UTR, kb2 is the defined
    length of downstream.
    '''
    f = Fasta(fa)
    with open(ann, 'r') as fh, open('3-UTR.fa', 'w') as out1, open('downstream.fa', 'w') as out2:
        mdict = {}
        ndict = {}
        for line in fh:
            # this is the demo line that we want to filter out
            # chr7    GLEAN   Gene    25420153        25421713        0.953889        -       .       Name=Pgl_GLEAN_10006696;
            if line.startswith('#'):
                continue
            new = line.strip().split('\t')
            if new[2] != 'CDS':
                continue
            gene = ge.GeneName(new[-1])
            if gene not in mdict:
                mdict[gene] = []
                ndict[gene] = []
            ndict[gene].append(new[0])
            ndict[gene].append(new[6])
            mdict[gene].append(int(new[3]))
            mdict[gene].append(int(new[4]))

        for gene in sorted(mdict):
            if ndict[gene][1] == '+':
                stop = max(mdict[gene])
                start1 = stop + 1
                stop1 = stop + (int(kb1) + 1)
                start2 = stop1 + 1
                stop2 = stop1 + (int(kb2) + 1)
                chrom = ndict[gene][0]
                k1 = str(f[chrom][start1:stop1])
                out1.write('>{0}-3UTR'.format(gene) + '\n')
                out1.write(k1 + '\n')
                k2 = str(f[chrom][start2:stop2])
                out2.write('>{0}-downstream'.format(gene) + '\n')
                out2.write(k2 + '\n')
            elif ndict[gene][1] == '-':
                start = min(mdict[gene])
                start1 = start - (int(kb1) + 1)
                stop1 = start - 1
                start2 = start1 - (int(kb2) + 1)
                stop2 = start1 - 1
                if start1 < 0:
                    start1 = 0
                if stop1 < 0:
                    stop1 = 0
                if start2 < 0:
                    start2 = 0
                if stop2 < 0:
                    stop2 = 0
                k1 = revcmp_str(str(f[chrom][start1:stop1]))
                out1.write('>{0}-3UTR'.format(gene) + '\n')
                out1.write(k1 + '\n')
                k2 = revcmp_str(str(f[chrom][start2:stop2]))
                out2.write('>{0}-downstream'.format(gene) + '\n')
                out2.write(k2 + '\n')

def cds(fa, ann):
    """
    Extracting and concenating cds sequences for given annotated genes.
    """
    f = Fasta(fa)
    with open(ann, 'r') as fh, open('CDS.fa', 'w') as out1:
        mdict = {}
        for line in fh:
            if line.startswith('#'):
                continue
            new = line.strip().split('\t')
            if new[2] != 'CDS':
                continue
            gene = ge.GeneName(new[-1])
            if gene not in mdict:
                mdict[gene] = []
            start1 = int(new[3])
            stop1 = int(new[4])
            mdict[gene].append((new[0], start1, stop1, new[6]))

        for i in sorted(mdict):
            k = ''
            for j in range(len(mdict[i])):
                chrom = mdict[i][j][0]
                start = mdict[i][j][1]
                stop = mdict[i][j][2]
                strand = mdict[i][j][3]
                if strand == "+":
                    k1 = str(f[chrom][start:stop])
                else:
                    k1 = revcmp_str(str(f[chrom][start:stop]))
                k += k1
            out1.write('>{0}-CDS'.format(i) + '\n')
            out1.write(k + '\n')

def intron(fa, ann):
    """
    Extracting and concenating intron sequences for given annotated genes.
    """
    f = Fasta(fa)
    with open(ann, 'r') as fh, open('intron.fa', 'w') as out1:
        mdict = {}
        ndict = {}
        for line in fh:
            if line.startswith('#'):
                continue
            new = line.strip().split('\t')
            if new[2] != 'CDS':
                continue
            gene = ge.GeneName(new[-1])
            if gene not in mdict:
                mdict[gene] = []
                ndict[gene] = [new[0], new[6]]
            start1 = int(new[3])
            stop1 = int(new[4])
            mdict[gene].append((start1, stop1))

        for i in sorted(mdict):
            k = ''
            slist = sorted(mdict[i])
            total = len(slist)
            for j in range(0, total - 1):
                chrom = ndict[i][0]
                start = slist[j][1] + 1
                stop = slist[j + 1][0] - 1
                strand = ndict[i][1]
                if strand == "+":
                    k1 = str(f[chrom][start:stop])
                else:
                    k1 = revcmp_str(str(f[chrom][start:stop]))
                k += k1
            out1.write('>{0}-intron'.format(i) + '\n')
            out1.write(k + '\n')
