#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from Bio import SeqIO
import numpy as np


def nucleotidefeature(fa, pos):
    """
    Calculating features of single and double nucleotide contents 
    for segmented genomic regions  
    """
    fasta_sequences = SeqIO.parse(open(fa), 'fasta')
    mdict = {}
    n = 0
    xlist = ['Gene']
    two = []
    for i in ['A', 'T', 'C', 'G']:
        xlist.append(i + '-' + pos)

    for i in ['A', 'T', 'C', 'G']:
        for j in ['A', 'T', 'C', 'G']:
            string = i + j
            two.append(string)
            xlist.append(string + '-' + pos)
            n += 1

    for fasta in fasta_sequences:
        fname, sequence = fasta.id, str(fasta.seq)
        sequence = sequence.upper()
        if '.' in fname:
            n = fname.split('.')
            fname = n[0] + '.' + n[1]
        else:
            fname = fname
        name = fname.split('-')[0]
        tlist = []
        a = sequence.count('A')
        t = sequence.count('T')
        c = sequence.count('C')
        g = sequence.count('G')
        n = sequence.count('N')
        tlist = [a, t, c, g]
        for i in two:
            tlist.append(sequence.count(i))
        # ignoring uncertainty nucleotides in the sequence
        tlist.append(len(sequence) - n)
        tlist = np.array(tlist)
        mdict[name] = tlist

    ndict = {}
    for i in sorted(mdict):
        if i not in ndict:
            ndict[i] = []
        for j in mdict[i][:-1]:
            ndict[i].append(str(j / float(mdict[i][-1])))

 #   fasta_sequences.close()
    # return the title and dictionary with gene name and appended values.
    return xlist, ndict


def merge(*args):
    '''
    Merge multiple features of each gene together. Each argument stands for the
    dictionary of the class of each feature for each gene
    '''
    mdict = {}

    # fine the common keys in multiple dictionaries
    common_keys = set(args[0].keys())
    for d in args[1:]:
        common_keys.intersection_update(set(d.keys()))

    # appending values through common_keys to a dictionary
    for i in common_keys:
        if i not in mdict:
            mdict[i] = []
        for d in args[:]:
            mdict[i].extend(d.get(i))

    return mdict
