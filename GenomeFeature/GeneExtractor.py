#!/usr/bin/env python
# -*- coding: UTF-8 -*-

def GeneName(args):
    """
    Defining which species the last column belonging to 
    and extracting gene name from the last column
    """
    if 'Pgl' in args:
        gene = args.split('=')[1].replace(';', '')
    elif 'Seita' in args or 'Pavir' in args:
        n = args.split(';')[0]
        m = n.replace('ID=', '').split('.')
        gene = m[0] + '.' + m[1]
    elif 'Zm' in args:
        n = args.split(';')[0]
        if '_T' in n:
            m = n.split('.')[0]  # ID=GRMZM5G800101_T01
            gene = m.replace('ID=', '').split('_')[0]
        else:
            gene = n.split('.')[0].replace('ID=', '')  # ID=GRMZM5G800101.RefGen_V4
    elif 'Sobic' in args:
        n = args.split(';')[1].split('=')[1].split('.')
        gene = n[0]+'.'+n[1]
    else:
        # expected format ID=gene;Name=gene;pacid=37943753
        n = args.split(';')
        for iind, i in enumerate(n):
            if i.startswith("Parent"):
                mindex = iind
        gene = n[mindex].replace('Parent=','')
    return (gene)
