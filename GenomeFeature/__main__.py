#!/usr/bin/env python

import argparse
from GenomeFeature import FeatureExtraction as fe
from GenomeFeature import GenomeSegment as gs
import argcomplete

__author__ = "Zhikai Liang"
__copyright__ = "Copyright 2020, GenomeFeature"
__license__ = "BSD 3.0"


def main():
    """
    Organizing and producing a matrix with calculated features for all genes
    """
    parser = argparse.ArgumentParser(description="generating genomic features")
    parser.add_argument('-a', '--annotation', required=True,
                        help='This is the primary genome annotation file')
#    parser.add_argument('-f', '--fullannotation', action='store_true',
#                        help='This is the genome annotation file with multiple transcripts')
    parser.add_argument('-g', '--genome', required=True,
                        help='This is the genome assembly sequence')
    parser.add_argument('-d', '--fiveutr', required=True,
                        help='This is the sequence length for 5utr')
    parser.add_argument('-u', '--threeutr', required=True,
                        help='This is the sequence length for 3utr')
    parser.add_argument('-e', '--extension', required=True,
                        help='This is the extended sequence length for both upstream and downstream')
    parser.add_argument('-o', '--outfile', required=True,
                        help='This is the output file storing features for each gene')
    argcomplete.autocomplete(parser)
#    if not (args.annotation or args.fullannotation):
#        parser.error('No action requested, add -a or -f')
    args = parser.parse_args()
    utr5 = int(args.fiveutr)
    utr3 = int(args.threeutr)
    ext = int(args.extension)

    print ('Starting to generate fasta file for upstream regions of single genes ...\n')
    gs.upstream(args.genome, args.annotation, utr5, ext)
    print ('Starting to generate fasta file for downstream regions of single genes ...\n')
    gs.downstream(args.genome, args.annotation, utr3, ext)
    print ('Starting to generate fasta file for CDS regions of single genes ...\n')
    gs.cds(args.genome, args.annotation)
    print ('Starting to generate fasta file for intron regions of single genes ...\n')
    gs.intron(args.genome, args.annotation)

    print ('Starting to calculate genome features in CDS region ...\n')
    e1, e2 = fe.nucleotidefeature('CDS.fa', 'CDS')
    tlist = ['gene']
    tlist.extend(e1[1:])
    print ('Starting to calculate genome features in intron region ...\n')
    i1, i2 = fe.nucleotidefeature('intron.fa', 'Intron')
    tlist.extend(i1[1:])
    print ('Starting to calculate genome features in 5utr region ...\n')
    f1, f2 = fe.nucleotidefeature('5-UTR.fa', '5UTR')
    tlist.extend(f1[1:])
    print ('Starting to calculate genome features in 3utr region ...\n')
    t1, t2 = fe.nucleotidefeature('3-UTR.fa', '3UTR')
    tlist.extend(t1[1:])
    print ('Starting to calculate genome features in gene upstream region ...\n')
    u1, u2 = fe.nucleotidefeature('upstream.fa', 'Up')
    tlist.extend(u1[1:])
    print ('Starting to calculate genome features in gene downstream region ...\n')
    d1, d2 = fe.nucleotidefeature('downstream.fa', 'Down')
    tlist.extend(d1[1:])
    print ('Merging features ...\n')
    ndict = fe.merge(e2, i2, f2, t2, u2, d2)
    print ('Generating the final file with all features ...\n')
    mylen = len(tlist) - 1
    with open(args.outfile, 'w') as out:
        out.write(','.join(tlist) + '\n')
        for i in sorted(ndict):
            # only save genes with full sets of features
            if len(ndict[i]) != mylen:
                continue
            out.write(i + ',' + ','.join(ndict[i]) + '\n')


if __name__ == "__main__":
    print ("##########==Using GenomeFeature to calculate Genomic Features per gene==####")
    print ("Calculating genomic features for given genes and associated sequences ...")
    print ("#############################################################################\n")
    main()
