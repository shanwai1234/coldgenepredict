# Cold responsive gene predictions across grass species
We proposed this ML framework to predict cold responsive genes across species using features derived from DNA sequences. AUC-ROCs of across-species cold-responsive gene predictions vary around 0.8 using studied species. One innovation of this frame is separating stress-responsive (cold in here) genes from expressed genes. This framework can potentially work on predicting cold responsive genes in other unknown species. As this study has only been tested on selected panicoid grasses, we have not tested its performance on remotely related species. To implement the model, you still need to split expressed genes from annotated genes to avoid the introduction of external noise as expressed genes own very distinct feature structure than unexpressed genes (Jacob DW et al. 2019; RC Sator et al. 2019). There are potentially two ways for identifying expressed genes: 1> using available RNA-seq data in your studied species and setting a threshold for genes (i.e. average FPKM > 1 per gene). This is what we performed in the paper; 2> using published ML framework (i.e. Jacob DW et al. 2019) for separating expressed genes and unexpressed genes and plugging expressed genes into our ML framework (this hasn't been tested in the paper).

**Data**: Genes with labels, cluster info, and features 

**DemoSeqAnno**: demo fasta and gff files for GenomeFeature 

**GenomeFeature**: a python-based function to extract sequence features 

**ML-models**: trained ML models from different species 

**R scripts**: R scripts for training ML models 

## 1. Genome Feature Generation

If your predicted genes are not in our studied species, you could prepare genomic sequence in fasta format and gff files with gene annotations.

### Dependencies
- [python >= 3.9](https://www.python.org)
- [BioPython](https://biopython.org/wiki/Download)
- ~~pyfasta~~ pyfasta has been disabled. Please install pyfaidx instead for sequence extraction
- [pyfaidx](https://anaconda.org/bioconda/pyfaidx)

### How to use
1.1 Download the package using git
```
git clone https://shanwai1234@bitbucket.org/shanwai1234/coldgenepredict.git
```
1.2 Run the code below

```
$ python -m GenomeFeature -a DemoSeqAnno/Sbicolor_454_v3.1.1.gene.primary.gff3 -g DemoSeqAnno/Sbicolor_454_v3.0.1.fa -d 500 -u 500 -e 2000 -o output.txt

-m Executing the module of GenomeFeature
-a Using gff file
-g using provided genome sequence file in the fasta format
-d 3' UTR length
-u 5' UTR length
-e extended region from both of UTRs to represent upstream and downstream sequences
-o output file name with extracted features
```
Note: As gene naming nomenclatures vary from species to species, if you find errors please contact the author for correction.

## 2. Data loading and model running

### Dependencies
```
# R >= 3.5
> install.packages(c("tibble","tidyr","caret","randomForest","tidyverse","PRROC","rlist"))
```
### How to use
2.1 Load functions
```
> source("R scripts/MLfunctions.R")
```
2.2 Run models based on the purpose in RunML.R

## 3. Predicting cold-responsive/noncold-responsive genes in unknown species

3.1 You could also use trained models to predict cold-responsive/noncold-responsive genes in unknown species. Following the guideline in [here](ML-models/README.md) 

### Contact authors for questions:
> Xiaoxi Meng: meng0170@umn.edu; Zhikai Liang: zliang@huskers.unl.edu; James Schnable: schnable@unl.edu
