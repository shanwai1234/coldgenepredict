# Deposited datasets

- GeneFeature.RDS: Sequence features for genes in maize, sorghum, foxtail millet, pearl millet, switchgrass and proso millet. Chromatin, diversity and evoluation features for maize genes. 
- SpeciesCluster.RDS: Cluster information for genes in 6 species above. Cluster per gene was assigned according to MCL results.
- Six text files: Genes with DEG(1)/non-DEG(0)labels with mean fpkm across replicates and conditions.
- cross-species-data-partition: 
   1> Six species: pooled genes after expression binning from six species with data partition under 5 random seeds;
   2> Six species plus external data: pooled genes after expression binning from six species plus external data with data partition under 5 random seeds
```
# read and extract information in .RDS files
> feature = readRDS("GeneFeature.RDS")
> cluster = readRDS("SpeciesCluster.RDS")

> new_data = binning(data)
> fd = feature$Total # all of sequence features for specific genes calculated from any of six species.
> cd = cluster$cluster
> write.table(...) # write to an output file with a random name if you want to train the model on different programs.
```